package ru.example.kafka.pattetn.kafkapattern.service;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;


@Slf4j
public abstract class AbstractKafkaConsumer<T> {
    private final ObjectMapper objectMapper;
    private final Class<T> tClass;

    protected AbstractKafkaConsumer(ObjectMapper objectMapper, Class<T> tClass) {
        this.objectMapper = objectMapper;
        this.tClass = tClass;
    }

    @KafkaHandler
    private void handleMessage(@Payload String message, @Header(KafkaHeaders.OFFSET) String offset) {
        try {
            T someDto = this.objectMapper.readValue(message, tClass);
            handle(someDto);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public abstract void handle(T someDto);
}
