package ru.example.kafka.pattetn.kafkapattern.service;

import org.springframework.core.annotation.AliasFor;
import org.springframework.kafka.annotation.KafkaListener;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@KafkaListener(autoStartup = SimpleKafkaListener.AUTO_START, containerFactory = SimpleKafkaListener.CONTAINER_FACTORY)
public @interface SimpleKafkaListener {
    String AUTO_START = "true";
    String CONTAINER_FACTORY = "consumer";
    @AliasFor(annotation = KafkaListener.class, attribute = "topics")
    String[] topics() default {};
}
