package ru.example.kafka.pattetn.kafkapattern.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class SimpleDto {
    private final Long orderId;
    private final String message;
}
