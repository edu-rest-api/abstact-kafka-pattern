package ru.example.kafka.pattetn.kafkapattern.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.example.kafka.pattetn.kafkapattern.dto.SimpleDto;

@Slf4j
@Service
@SimpleKafkaListener(topics = "kafka-test")
public class KafkaConsumer extends AbstractKafkaConsumer<SimpleDto> {

    protected KafkaConsumer(ObjectMapper objectMapper) {
        super(objectMapper, SimpleDto.class);
    }

    @Override
    public void handle(SimpleDto simpleDto) {
        log.info("message: {}", simpleDto);
    }
}
